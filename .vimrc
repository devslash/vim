set nocompatible  	" Required for vundle

set relativenumber 	" Adds relative numbering	
set number		" Sets the current line to have its number
filetype off

" Key mappings
map <F8> :tabn<CR>
map <F7> :tabp<CR>

set rtp+=~/.vim/bundle/Vundle.vim

" Start plugin requirements
call vundle#begin()

Plugin 'gmarik/Vundle.vim'

Plugin 'tpope/vim-fugitive'

call vundle#end()
filetype plugin indent on
